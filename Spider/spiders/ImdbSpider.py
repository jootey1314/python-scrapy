# coding:utf-8
from imp import reload
import sys
import json

reload(sys)
sys.setdefaultencoding("utf-8")

from scrapy.spiders import CrawlSpider, Request, Rule
from Spider.items import ImdbItem
from scrapy.linkextractors import LinkExtractor


class ImdbSpider(CrawlSpider):
    name = 'imdb'
    allowed_domains = ['www.imdb.cn']
    rules = (
        Rule(LinkExtractor(allow=r"/title/tt\d+$"), callback="parse_imdb", follow=True),
    )

    def start_requests(self):
        for i in range(1, 14623):
            url = "http://www.imdb.cn/nowplaying/" + str(i)
            print(url)
            yield Request(url=url, callback=self.parse)

    @staticmethod
    def parse_imdb(response):
        item = ImdbItem()
        title = " ".join(response.xpath('//*[@class="fk-3"]/div[@class="hdd"]/h3/text()').extract())
        item['title'] = title  # json.dumps(title, ensure_ascii=False).decode('utf8')
        item["url"] = response.url
        baseStr = '//*[@class="fk-3"]/div[@class="bdd clear"]/ul/li'
        item["actor"] = " ".join(response.xpath(baseStr + '[3]/a/text()').extract())
        item["country"] = " ".join(response.xpath(baseStr + '[7]/a[1]/text()').extract())
        item["playTime"] = " ".join(response.xpath(baseStr + '[6]/a[1]/text()').extract())
        item["movieType"] = " ".join(response.xpath(baseStr + '[6]/a[2]/text()').extract())
        item["toStar"] = " ".join(response.xpath(baseStr + '[4]/a/text()').extract())
        item["language"] = " ".join(response.xpath(baseStr + '[@class="nolink"]/a/text()').extract())
        yield item
