# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SpiderItem(scrapy.Item):
    diaper_name = scrapy.Field()
    diaper_price = scrapy.Field()
    diaper_url = scrapy.Field()
    diaper_source_shop = scrapy.Field()
    # print("item 数据:"+title+" "+link+" "+desc)
    pass


class ImdbItem(scrapy.Item):
    url = scrapy.Field()  # url
    title = scrapy.Field()  # 影片名
    actor = scrapy.Field()  # 导演
    country = scrapy.Field()  # 国家
    playTime = scrapy.Field()  # 上映时间
    movieType = scrapy.Field()  # 影片类型
    toStar = scrapy.Field()  # 主演
    language = scrapy.Field()  # 语言
