# coding=utf-8

import re
from scrapy.selector import Selector
from scrapy.spiders import Spider
from scrapy_splash import SplashRequest
from Spider.items import SpiderItem


class VipShopDiaper(Spider):
    name = 'VipShopSpider'
    allowed_domains = ["category.vip.com"]
    start_url = 'https://category.vip.com/suggest.php?keyword=%E7%BA%B8%E5%B0%BF%E8%A3%A4'
    base_url = 'https://category.vip.com/suggest.php?keyword=%E7%BA%B8%E5%B0%BF%E8%A3%A4'
    script = """
               function main(splash, args)
                  splash:go(args.url)
                  local scroll_to = splash:jsfunc("window.scrollTo")
                  scroll_to(0, 2800)
                  splash:set_viewport_full()
                  splash:wait(5)
                  return {html=splash:html()}
                end
              """

    def start_requests(self):
        yield SplashRequest(url=self.start_url, callback=self.parse, endpoint='execute',
                            args={'lua_source': self.script})
        # response = Service.parseBySplashServer(self.start_url)
        # yield self.parse(self, response)

    def parse(self, response):
        # response= Service.parseBySplashServer()
        # sel = Selector(response)
        sel = response.selector
        items = sel.xpath('//div[@class="goods-list-item  c-goods  J_pro_items"]')

        # itemss = sel.xpath('//div[re:findall(@id,"J_pro_")]')

        for data in items:
            diaper_name = data.xpath('.//h4[@class="goods-info goods-title-info"]/a/@title').extract_first()
            diaper_price = data.xpath(
                './/div[@class="goods-price-wrapper"]/em/span[@class="price"]/text()').extract_first()
            diaper_url = data.xpath('.//h4[@class="goods-info goods-title-info"]/a/@href').extract_first()

            shop_diaper_item = SpiderItem()
            shop_diaper_item['diaper_name'] = diaper_name
            shop_diaper_item['diaper_price'] = (re.findall(r"\d+\.?\d*", diaper_price))[0]
            shop_diaper_item['diaper_url'] = 'https:' + diaper_url
            shop_diaper_item['diaper_source_shop'] = '唯品会'

            # print("输出:" + shop_diaper_item['diaper_name'])
            yield shop_diaper_item

        next_url = sel.xpath(
            '//div[@class="m-cat-paging ui-paging"]/a[@class="cat-paging-next next"]/@href').extract_first()

        if next_url is not None:
            next_url = response.urljoin(next_url)
            yield SplashRequest(next_url, callback=self.parse, endpoint='execute', args={'lua_source': self.script})
